const getGreeting = require("./greeting");
const getWeather = require("./weather");

module.exports = {
  getGreeting,
  getWeather,
};
